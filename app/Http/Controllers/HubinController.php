<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Siswa;
use App\Models\Pemetaan;
use Barryvdh\DomPDF\PDF;
use App\Models\Perusahaan;
use App\Imports\UserImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class HubinController extends Controller
{

    public function dashboardhubin(){
        $perusahaan = Perusahaan::all();
        $siswa      = Siswa::all();
        return view('hubin.dashboardhubin', [
            'title' =>  'Dashboard | Hubin',
            'titleheader'   =>  'Dashboard',
            'perusahaan'    =>  $perusahaan,
            'siswa'         =>  $siswa
        ]);
    }
    public function index(){
        return view('perusahaanhubin', [
            'title' => 'Daftar Perusahaan'
        ]);
    }

    public function hubinperusahaan(){
        $perusahaan = Perusahaan::all();
        return view('hubin.perusahaanhubin', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan',
            'perusahaan'    =>  $perusahaan
        ]);
    }

    public function hubinpemetaan(){
        return view('hubin.pemetaanpkl', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan'
        ]);
    }

    public function hubineditakunsiswa (){
        return view('hubin.editakunsiswa', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan'
        ]);
    }

    public function siswaterdaftarhubin(){
        return view('hubin.siswaterdaftarhubin', [
            'title' =>  'Hubin | Siswa Terdaftar',
            'titleheader'   =>  'Daftar Perusahaan'
        ]);
    }
    
    public function daftarsiswahubin(){
        $siswa = Siswa::with('perusahaan')->get();
        return view('hubin.daftarsiswahubin', [
            'title' =>  'Hubin | Daftar Siswa',
            'titleheader'   =>  'Daftar Siswa',
            "siswa" => $siswa,
        ]);
    }

    public function cetaksurat(){
        $perusahaan = Perusahaan::all();
        $data = User::all();
        return view('hubin.cetaksurat', [
            'title' =>  'Hubin | Cetak Surat',
            'titleheader'   =>  'Cetak Surat',
            'perusahaan'    =>  $perusahaan,
            'data'          =>  $data
        ]);
    }

    public function importdata(Request $request){
        $file = $request->file('file');

		Excel::import(new UserImport, $file);

        return back()->withStatus('Excel File Berhasil Diimport!');
    }
    public function tampilimport(){
        $user = User::all();
        return view('hubin.upload', compact('user'));
    } 

    public function cetakmurid(){
        $data = User::all();

        $pdf = PDF::loadView('hubin.surat');

        return $pdf->download('suratpengajuan.pdf');
    }
}
