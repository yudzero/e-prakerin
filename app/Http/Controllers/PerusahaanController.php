<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PerusahaanController extends Controller
{
    public function pembimbingperusahaan(){
        return view('pembimbing-perusahaan.pembimbing-perusahaan', [
            'title' =>  'Dashboard | Pembimbing Perusahaan',
            'titleheader'   =>  'Pembimbing Perusahaan'
        ]);
    }

    public function detaildata(){
        return view('detail-data.detail-data', [
            'title' => 'Detail Data',
            'titleheader' => 'Detail Data'
        ]);
    }

    public function muridsudahabsen(){
        return view('murid-sudah-absen.murid-sudah-absen', [
            'title' => 'Sudah Absen',
            'titleheader' => 'Sudah Absen'
        ]);
    }
}
