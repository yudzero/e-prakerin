<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function GuzzleHttp\Promise\all;

class SiswaController extends Controller
{
    public function jurnalsiswa(){
        return view('siswa.jurnalsiswa', [
            'title' =>  'Siswa | Jurnal Siswa',
            'titleheader'   =>  'Jurnal Siswa'
        ]);
    }

    public function sikapsiswa(){
        return view('siswa.sikapsiswa', [
            'title' =>  'Siswa | Sikap Siswa',
            'titleheader'   =>  'Sikap Siswa'
        ]);
    }

    public function daftarindustrisiswa(){
        return view('siswa.daftarindustri-siswa', [
            'title' =>  'Siswa | Daftar Industri',
            'titleheader'   =>  'Daftar Industri'
        ]);
    }

    public function profilsiswa(Siswa $siswa){
        $siswa = Siswa::all()->first();
        return view('siswa.profilsiswa', [
            'title' =>  'Siswa | Profil Siswa',
            'titleheader'   =>  'Profil Siswa',
            'siswa'          =>  $siswa
        ]);
    }

    public function updateprofil(Request $request, Siswa $siswa){
        $attr = $request->validate([
            'NamaSiswa' =>  ['string', 'min:3', 'max:191', 'required'],
            'email' =>  ['email', 'string', 'min:3', 'max:191', 'required'],
            'NoTelp' =>  ['string', 'min:3', 'max:191', 'required'],
        ]);

        $siswa->update($attr);
 
            return back()->with('message', 'Profil Sukses Di Update');
    }

    public function berandasiswa(){
        return view('siswa.berandasiswa', [
            'title' =>  'Siswa | beranda Siswa',
            'titleheader'   =>  'beranda Siswa'
        ]);

        
    }

    public function edit(){
        return view('siswa.profilsiswa', [
            'title' =>  'Siswa | Profil Siswa',
            'titleheader'   =>  'Profil Siswa'
        ]);
    }

    public function update(Request $request){
        $attr = $request->validate([
            'username'  => ['string']
        ]);

        auth()->user()->update($attr);
    }

}
