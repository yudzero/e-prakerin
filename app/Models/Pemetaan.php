<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemetaan extends Model
{
    use HasFactory;

    protected $fillable = ['id_pendaftaran', 'id_periode', 'NoPerusahaan', 'nis', 'nip', 'id_pembimbing'];
    protected $table = 'pemetaan';
    protected $primaryKey = 'id_pendaftaran';
    private $increment = false;

    public function getForeignKey()
    {
        return $this->primaryKey;
    }

    public function siswa(){
        return $this->hasOne(Siswa::class, 'nis', 'nis');
    }

    public function perusahaan(){
        return $this->belongsTo(Perusahaan::class, 'NoPerusahaan', 'NoPerusahaan');
    }
}
