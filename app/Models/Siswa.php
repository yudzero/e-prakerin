<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;

    protected $fillable=['nama', 'kelas', 'email', 'NoTelp'];

    protected $table = 'siswas';

    protected $primaryKey = 'nis';

    private $increment = false;

    public function getForeignKey()
    {
        return $this->primaryKey;
    }

    public function perusahaan(){
        return $this->hasOneThrough(Perusahaan::class, Pemetaan::class, 'NoPerusahaan');
    }


    public function pemetaan(){
        return $this->hasOne(Pemetaan::class);
    }

    // public function getCreatedAtAttribute(){
    //     return Carbon::parse($this->attributes['Tgllahir'])
    //     ->translatedFormat('l, d F Y');
    // }
}
