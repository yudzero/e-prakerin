@extends('layouts.main')
@section('content')


    <section>
        <div class="content-body">
            <div class="container mb-5">
                <div class="card">
                    <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Murid</p> 
                    <table class="tabelperusahaan mb-5 mt-3">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Nama Perusahaan</th>
                        </tr>
                        
                    @foreach ($siswa as $s)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $s->NamaSiswa }}</td>
                            <td>{{ $s->kelas }}</td>
                            <td>{{ $s->perusahaan->NamaPerusahaan ?? 'Scola'}}</td>
                        </tr>
                        @endforeach
                    
                    </table>
                    <hr>
                    <p class="teks1">Rows per page: <span class=""> 8 <i class="fa-solid fa-caret-down"></i><span class="teks1 ml-4">1-8 of 1240</span><span><i class="fa-solid fa-chevron-left mr-3"></i><i class="fa-solid fa-chevron-right"></i> </span></p>
                </div>
            </div>
        </div>
    </section>

@endsection