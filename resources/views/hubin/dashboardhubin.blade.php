@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <a style="text-decoration: none;" href="#">
                <div class="card">
                    <p class="mt-3" style="text-align: center;">Jumlah Perusahaan</p>
                    <h3 class="mb-3" style="font-weight:bold; color:black; text-align:center;">{{ $perusahaan->count() }}</h3>
                </div></a>
            </div>
            <div class="col">
                <div class="card">
                    <p class="mt-3" style="text-align: center;">Jumlah Murid</p>
                    <h3 class="mb-3" style="font-weight:bold; color:black; text-align:center;">{{ $siswa->count() }}</h3>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <p class="mt-3" style="text-align: center;">Siswa Terdaftar</p>
                    <h3 class="mb-3" style="font-weight:bold; color:black; text-align:center;">70</h3>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <p class="mt-3" style="text-align: center;">Siswa Belum Terdaftar</p>
                    <h3 class="mb-3" style="font-weight:bold; color:black; text-align:center;">155</h3>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col">
                <div class="card">
                    <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Murid</p> 
                    <table class="tabelperusahaan mb-5 mt-3">
                        
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Nama Perusahaan</th>
                            <th>Divisi</th>
                        </tr>
                        
                        @foreach($siswa as $s)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $s->NamaSiswa }}</td>
                            <td>{{ $s->kelas }}</td>
                            <td>{{ $s->perusahaan->NamaPerusahaan ?? 'Gada' }}</td>
                            <td>IT</td>
                        </tr>
                       @endforeach
                    
                    </table>
                    <hr>
                    <p class="teks1">Rows per page: <span class=""> 8 <i class="fa-solid fa-caret-down"></i><span class="teks1 ml-4">1-8 of 1240</span><span><i class="fa-solid fa-chevron-left mr-3"></i><i class="fa-solid fa-chevron-right"></i> </span></p>
                </div>
            </div>

            <div class="col">
                <div class="card">
                    <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Perusahaan</p> 
                    <table class="tabelperusahaan mb-5 mt-3">
                        <tr>
                            <th>Nama Perusahaan</th>
                            <th>Jumlah Murid</th>
                        </tr>
                        
                    @foreach($perusahaan as $p)
                        <tr>
                            <td>{{ $p->NamaPerusahaan }}</td>
                            <td>5</td>
                        </tr>
                    @endforeach
                    
                    </table>
                    <hr>
                    <p class="teks1">Rows per page: <span class=""> 8 <i class="fa-solid fa-caret-down"></i><span class="teks1 ml-4">1-8 of 1240</span><span><i class="fa-solid fa-chevron-left mr-3"></i><i class="fa-solid fa-chevron-right"></i> </span></p>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection