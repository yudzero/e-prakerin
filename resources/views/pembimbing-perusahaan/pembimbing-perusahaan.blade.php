@extends('layouts.main')
@section('content')
   <div class="container">
        <div class="row mt-4">
            <div class="col-4">
                <div class="card">
                    <a href="" style="text-decoration: none; color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Jumlah Siswa</h5>
                    <h1 class="mb-4" style="text-align: center;">45</h1>
                    </a>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <a href="" style="text-decoration: none; color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Belum Absen</h5>
                    <h1 class="mb-4" style="text-align: center;">20</h1>
                    </a>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <a href="" style="text-decoration: none; color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Sudah Absen</h5>
                    <h1 class="mb-4" style="text-align: center;">25</h1>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-9">
                
            </div>
            <div class="col-3">

            </div>
        </div>
    </div>
@endsection