<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HubinController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\SekolahController;
use App\Http\Controllers\PerusahaanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route Hubin */
Route::get('/', function () {
    return view('welcome', [
        "title" =>  "Dashboard | Hubin",
        "titleheader" =>  "Dashboard"
    ]);
});




/* Route Siswa */



/* Route Pembimbing Perusahaan */
Route::get('/perusahaan/detaildata', [PerusahaanController::class, 'detaildata'])->name('detaildata');
Route::get('/perusahaan/sudahabsen', [PerusahaanController::class, 'sudahabsen'])->name('sudahabsen');


/* Route Pembimbing Sekolah */
Route::get('/evaluasipkl', [SekolahController::class, 'evaluasipkl'])->name('evaluasipkl');
Route::get('/sekolah/sikap', [SekolahController::class, 'viewsikapsiswa'])->name('viewsikapsiswa');
Route::get('/sekolah/siswa', [SekolahController::class, 'daftarsiswasekolah'])->name('daftarsiswasekolah');


// Route::get('/hubin/perusahaan', [HubinController::class, 'index']);

/*Route Login*/
Route::get('/login', [LoginController::class, 'viewlogin'])->name('viewlogin');

Route::post('/postlogin', [LoginController::class, 'postlogin'])->name('postlogin');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::middleware(['auth','ceklevel:hubin'])->group(function(){
    route::get('/dashboard/hubin', [HubinController::class, 'dashboardhubin'])->name('dashboardhubin');
    Route::get('/hubin/perusahaan', [HubinController::class, 'hubinperusahaan'])->name('hubinperusahaan');
    Route::get('/hubin/editakunsiswa', [HubinController::class, 'hubineditakunsiswa'])->name('hubineditakunsiswa');
    Route::get('/hubin/pemetaan', [HubinController::class, 'hubinpemetaan'])->name('hubinpemetaan');
    Route::get('/hubin/siswaterdaftar', [HubinController::class, 'siswaterdaftarhubin'])->name('siswaterdaftarhubin');
    Route::get('/hubin/siswa', [HubinController::class, 'daftarsiswahubin'])->name('daftarsiswahubin');
    Route::get('/hubin/cetaksurat', [HubinController::class, 'cetaksurat'])->name('cetaksurat');
    Route::get('cetakmurid', [HubinController::class, 'cetakmurid'])->name('data.pdf');
    Route::post('/hubin/importdata', [HubinController::class, 'importdata'])->name('importdata');
    Route::get('/hubin/importdata', [HubinController::class, 'tampilimport'])->name('tampilimport');
});

Route::middleware(['auth','ceklevel:siswa'])->group(function(){
    Route::get('/dashboard/siswa', [SiswaController::class, 'berandasiswa'])->name('berandasiswa');
    Route::get('/siswa/jurnal', [SiswaController::class, 'jurnalsiswa'])->name('jurnalsiswa');
    Route::get('/siswa/sikap', [SiswaController::class, 'sikapsiswa'])->name('sikapsiswa');
    Route::get('/siswa/daftarpkl', [SiswaController::class, 'daftarindustrisiswa'])->name('daftarindustrisiswa');
    Route::get('/siswa/profil', [SiswaController::class, 'profilsiswa'])->name('profilsiswa');
    Route::get('edit', [SiswaController::class, 'edit'])->name('profile.edit');
    Route::put('update', [SiswaController::class, 'update'])->name('profile.update');
    Route::get('/siswa/beranda', [SiswaController::class, 'berandasiswa'])->name('berandasiswa');
});

Route::middleware(['auth','ceklevel:pembimbing perusahaan'])->group(function(){
    Route::get('/dashboard/pembimbingperusahaan', [PerusahaanController::class, 'pembimbingperusahaan'])->name('pembimbingperusahaan');
});
Route::middleware(['auth','ceklevel:pembimbing sekolah'])->group(function(){
    Route::get('/dashboard/pembimbingsekolah', [SekolahController::class, 'evaluasipkl'])->name('evaluasipkl');
});
